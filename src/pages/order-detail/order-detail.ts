import { Component } from '@angular/core';

import { 
  IonicPage, 
  NavController, 
  NavParams, 
  LoadingController, 
  ActionSheetController 
} from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { OrderProvider } from '../../providers/order/order';

/**
 * Generated class for the OrderDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {

  segmentSelected: string = "order_info";
  orderDetailItems: any;
  selectedItem: any;
  segments: string[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private orderProvider: OrderProvider,
    private loadingCtrl: LoadingController,
    private barcodeScanner: BarcodeScanner,
    private actionSheetCtrl: ActionSheetController
  ) {
    this.segmentSelected = 'order_info';
    this.segments = ['T2','T2','O1','K1'];

    console.log(this.segments.length);
    this.populateData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderDetailPage');
  }

  populateData(){
    let loader = this.loadingCtrl.create({
      content: "กำลังโหลด..."
    });
    loader.present();

    try{
      this.orderProvider.getOrderList().subscribe(data => {
        this.orderDetailItems = data.filter(function(s){return s.id < 4;});

         for(var i = 0; i<this.orderDetailItems.length; i++){
           this.orderDetailItems[i].checked = false;
           this.orderDetailItems[i].email = '';
         }

        loader.dismiss();
      });
    }catch(e){
      loader.dismiss();
      throw e;
    }finally{
      
    }
  }

  orderDetailTapped(event, item) {

    item.checked = !item.checked;
    return;

    //this.selectedItem = item;
    //this.doScan();
  }

  doScan(){
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode : ', barcodeData.text);
      if (barcodeData.text.trim() == ''){
        return;
      }

      if (this.selectedItem == null){
        this.selectedItem = this.orderDetailItems.filter((item) => {
          return (item.barcode.toLowerCase().indexOf(barcodeData.text.toLowerCase()) > -1);
        })
      }

      if (this.selectedItem != null){
        console.log(this.selectedItem.id);
      }

      // if (barcodeData.text == '8850039207565'){
      //   this.selectedItem.checked = true;
      // }
      this.selectedItem.email = barcodeData.text;
      this.selectedItem.checked = !this.selectedItem.checked;

      this.selectedItem = null;
     }).catch(err => {
         console.log('Error', err);
     });
  }

  getItem(){
    return this.orderDetailItems
  }

  doPrint(){
    
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Action List',
      buttons: [
        {
          text: 'บันทึกและส่งข้อมูล',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'บันทึกแบบร่าง',
          role: 'destructive',
          handler: () => {
            console.log('Archive clicked');
          }
        },{
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
