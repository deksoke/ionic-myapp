import { Component } from '@angular/core';
import { IonicPage, Platform , NavController, NavParams, LoadingController } from 'ionic-angular';
import { OrderProvider } from '../../providers/order/order';

import { OrderDetailPage } from '../order-detail/order-detail';

@IonicPage()
@Component({
  selector: 'page-order-list',
  templateUrl: 'order-list.html',
})
export class OrderListPage {

  docStatus: string = "incomming";
  isAndroid: boolean = false;
  orders: any;
  searchValue: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private platform: Platform,
    private orderProvider: OrderProvider,
    private loadingCtrl: LoadingController
  ) {
    this.isAndroid = platform.is('android');
    this.populateData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderListPage');
  }

  orderTapped(event, item) {
    this.navCtrl.push(OrderDetailPage, {
      item: item
    });
  }

  populateData(){
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();

    try{
      this.orderProvider.getOrderList().subscribe(data => this.orders = data);
    }catch(e){
      throw e;
    }finally{
      loader.dismiss();
    }
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    refresher.complete();

    this.searchValue = '';
    this.populateData();

    // setTimeout(() => {
    //   console.log('Async operation has ended');
    //   refresher.complete();
    // }, 2000);
  }

  getItems(ev) {
    // Reset items back to all of the items
    //this.populateData();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.orders = this.orders.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }else{
      this.populateData();
    }
  }

}
