import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the ScanbarcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scanbarcode',
  templateUrl: 'scanbarcode.html',
})
export class ScanbarcodePage {

  barcodeNo: string;
  loading: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private barcodeScanner: BarcodeScanner,
    private http: HttpClient,
    public alertController: AlertController,
    public loadingController: LoadingController
  ) {
    this.barcodeNo = '';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanbarcodePage');
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      spinner: null,
      content: 'Loading...',
      showBackdrop: true
    });
    await this.loading.present();
  }

  async presentAlert(msg: string) {
    const alert = await this.alertController.create({
      title: 'Alert',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentAlertConfirm(data: RequestResult) {
    const alert = await this.alertController.create({
      title: `<strong>${data.Data.FirstName}</strong>`,
      message: `${data.Data.EventName}<br/>ต้องการลงทะเบียนใช่หรือไม่?`,
      buttons: [
        {
          text: 'ไม่',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ใช่',
          handler: () => {
            this.doRegister(data);
          }
        }
      ]
    });

    await alert.present();
  }

  doScan(){
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.barcodeNo = barcodeData.text;

      // this.presentAlert(barcodeData.text);

      //get information
      this.presentLoading();
      this.http.get('https://api.inno4farmers.com/api/ev/get-invite-by-code?code=' + barcodeData.text)
      .subscribe((data: RequestResult) => {

        this.loading.dismiss();
        if (data.Status === 'SUCCESS') {
          this.presentAlertConfirm(data);
        } else if (data.Status === 'NOTFOUND') {
          this.presentAlert('ไม่พบข้อมูลบัตรเชิญนี้');
        } else if (data.Status === 'REGISTERED') {
          this.presentAlert('ท่านได้ลงทะเบียนเรียบร้อยแล้ว');
        }

      }, error => {
        this.loading.dismiss();
      });

      


      
     }).catch(err => {
         console.log('Error', err);
     });
  }

  doConfirm(data: RequestResult) {

  }

  doRegister(data: RequestResult){
    //register
    this.http.get('https://api.inno4farmers.com/api/ev/register-by-mobile?code=' + data.Code)
    .subscribe((data: string) => {
      this.presentAlert(data);
    });
  }
}

export class RequestResult {
  Code: string;
  Status: string;
  Data: InviteMemberData;
}

export class InviteMemberData {
  Member_ID: number;
  Activity_ID: number;
  EventName: string;
  ActivityName: string;
  FullName: string;
  FirstName: string;
  LastName: string;
  Tel: string;
  Email: string;
}