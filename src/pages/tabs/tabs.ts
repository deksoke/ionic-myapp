import { Component } from '@angular/core';

// import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
// import { HistoryPage } from '../history/history';
import { OrderListPage } from '../order-list/order-list';
import { AccountPage } from '../account/account';
import { ScanbarcodePage } from '../scanbarcode/scanbarcode';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = OrderListPage;
  tab3Root = ScanbarcodePage;

  constructor() {

  }
}
