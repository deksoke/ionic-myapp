import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the OrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderProvider {

  constructor(public http: Http) {
    console.log('Hello OrderProvider Provider');
  }

  getOrderList(){
    return this.http.get('https://jsonplaceholder.typicode.com/users')
      .catch(this.formatErrors)
      .map(data => data.json());
  }

  private formatErrors(error: any) {
    return Observable.throw(error.json());
  }

}
