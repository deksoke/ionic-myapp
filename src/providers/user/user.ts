import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: HttpClient) {
    console.log('Hello UserProvider Provider');
  }

  getUserList(){
    return this.http.get('https://jsonplaceholder.typicode.com/users')
      .catch(this.formatErrors)
      .map(data => data.json());
  }

  private formatErrors(error: any) {
    return Observable.throw(error.json());
  }
}
